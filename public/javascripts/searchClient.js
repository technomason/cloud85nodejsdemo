/**
 * Created by Joshua Remy on 4/9/15.
 */

//Add Page Load Inits, like button bonding
$(function() {

    //Search Icon Select
    $("#searchBut").bind( "click", function() {
        if ($('#searchInput').val()!=""){

            search($('#searchInput').val());
        }

    });

    //Hit Enter in Box
    $('#searchInput').keypress(function (e) {
        if (e.which == 13) {
            if ($('#searchInput').val()!=""){
                search($('#searchInput').val());
            }
            return false;
        }

    });

});

function search(query) {
    console.log("Searching using "+ query);
    $("#searchResults").load("/search?qry="+query, function(responseTxt, statusTxt, xhr){
        if(statusTxt == "success"){
            console.log("Results "+ responseTxt);
            $("#searchResults").html(responseTxt);
        }


        if(statusTxt == "error") {
            console.log(statusTxt);
            alert("Errors: " + xhr.status + ": " + xhr.statusText);
        }


    });


}