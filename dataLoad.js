/**
 * Created by stash on 4/9/15.
 */
var http = require('http');

//Load Files Sync
var config = require('./config.json');
var settings = require('./indexSettings.json');
var data = require('./sampleData.json');
var SRCHHOST=config.search_host;
var SRCHHOST_PORT=config.search_port;

function httpCall(options,bodyStr,callback)  {

    var srchReq;
    srchReq = http.request(options, function (res) {
        res.setEncoding('utf8');
        var responseBody = "";
        res.on('data', function (chunk) {

            responseBody=responseBody+chunk;
        });

        res.on('end', function (chunk) {
            //Done with Response...load data
           // console.log(responseBody);
            callback(responseBody);
        });
    });
    srchReq.write(bodyStr);
    srchReq.end();

}

function createIndex(callback) {

    var options = {
        host: SRCHHOST,
        port: SRCHHOST_PORT,
        path: '/cloud/',
        method: 'PUT'
    };

    httpCall(options,JSON.stringify(settings),callback);



}

function deleteIndex(callback) {

    var options = {
        host: SRCHHOST,
        port: SRCHHOST_PORT,
        path: '/cloud/',
        method: 'DELETE'
    };

    httpCall(options,"",callback);


}

function loadData() {

    var options = {
        host: SRCHHOST,
        port: SRCHHOST_PORT,
        path: '/cloud/event',
        method: 'POST'
    };

    data.events.forEach(function(loadMe) {

        httpCall(options, JSON.stringify(loadMe), function (body) {
            console.log(body);
        });

    });

}

/*
Initial load function to delete, create and index the docs
 */
function load() {
    deleteIndex(function(){
        createIndex(function() {
            loadData()
        });
    });

}


load();

