var express = require('express');
var router = express.Router();
var http = require('http');
var config = require('./../config.json');
var settings = require('./../indexSettings.json');
var data = require('./../sampleData.json');

var SRCHHOST=config.search_host;
var SRCHHOST_PORT=config.search_port;

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Cloud 85 Event Search' });
});

/* GET Search Results page. */
router.get('/search', function(req, res, next) {

  var qryParam = req.query.qry;

  search(qryParam,function(result){
    res.render('searchResults',{data:JSON.parse(result)} );

  });


});

function search(query,callback) {

  var options = {
    host: SRCHHOST,
    port: SRCHHOST_PORT,
    path: '/cloud/event/_search?q='+query,
    method: 'GET'
  };
  var srchReq;

  srchReq = http.request(options, function (res) {
    res.setEncoding('utf8');
    var responseBody = "";
    res.on('data', function (chunk) {
      //Get Any Data
      responseBody=responseBody+chunk;
    });

    res.on('end', function (chunk) {
      //Done with Response...load data
      console.log(responseBody);
      callback(responseBody);
    });
  }).end();

}

module.exports = router;
