# Welcome

The following project was to demonstrate NodeJS and Elastic Search together at the Cloud85 Co-Working space.

## Setup Node JS
- Install [nodejs.org](http://nodejs.org)

## Setup Elastic
- Install Java for [Elastic](http://www.elastic.co/)
- Download and Extract [Elastic](http://www.elastic.co/)

## Download an IDE to use
- WebStorm or some other IDE

## Check out the code here

## Setup Project
Run the Node package installer in the root of the project directory:

```
#!shell

npm install
```
Edit the config.json file in the root of the project and add your Elastic Search host and port, usually 9200.  If localhost, leave it and all will be good.

### Run the Load Data
Load test data into the Elastic Search engine.
```
#!shell
node loadData.js
```

## Start up the application.
```
#!shell
node bin/www
```